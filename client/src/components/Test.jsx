import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { login } from '../login';

const Display = styled.div`
  background: #f0f0f0;
  color: #dadada;
  box-shadow: 1px 1px 3px rgba(54, 54, 54, 0.25);
  padding: 15px;
  text-align: center;
  font-size: 14px;
`;

const All = () => (
  <div>
    <One />
    <Two />
    <Three />
  </div>
);

export default All;

export const One = () => <Display><Link to="/one">One</Link></Display>;
export const Two = () => <Display><Link to="/two">Two</Link></Display>;

export const Three = () => {
  const onLogin = () => {
    login();
  };

  return (
    <Display>
      <Link to="/three">Three</Link>
      <button onClick={onLogin}>login</button>
    </Display>
  );
};
