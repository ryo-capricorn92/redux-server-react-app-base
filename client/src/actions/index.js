export const setUser = user => ({
  type: 'SET_USER',
  user,
});

export const test = testing => ({
  type: 'TEST',
  testing,
});
