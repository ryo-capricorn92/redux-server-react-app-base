/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';
import styled from 'styled-components';

import { setUser } from './actions';
import discord from './login';

import Test, { One, Two, Three } from './components/Test';

const Container = styled.div`
  text-align: center;
`;

class App extends React.Component {
  constructor(props) {
    super(props);
    discord.init(props.setUser);
  }

  render() {
    return (
      <BrowserRouter>
        <Container>

          <Route exact path="/" component={Test} />
          <Route path="/one" component={One} />
          <Route path="/two" component={Two} />
          <Route path="/three" component={Three} />
        </Container>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  setUser: (user) => { dispatch(setUser(user)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
/* eslint-enable react/jsx-filename-extension */
